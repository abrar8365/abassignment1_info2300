# ABAssignment1_INFO2300

This repository contains a HTML static website about popular Indian Cuisine.

# Setup Locally
## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. 
If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, 
see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or 
you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line]
(https://confluence.atlassian.com/x/NQ0zDQ).

## Run the project Locally
Open the project in file explorer where you cloned the repository.
Right-click the index.html file and open in any browser to view the site. 

# License

This project is licensed under the MIT License which you can find in LICENSE.md file. It is a permissive license that allows the person to obtain a copy of the software 
with right to use under the condition to include the copyright notice in all the copies. 

Permissions:
 Commercial use
 Modification
 Distribution
 Private use